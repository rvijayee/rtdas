window.adminUser = true;
var app = angular.module('rtdas',["ui.router","ui.grid","rtdasTemplates","ui.grid.pagination"]).config(["$stateProvider","$urlRouterProvider",
	function($stateProvider,$urlRouterProvider){

	$stateProvider

		.state('dashboard',
				{
					url:"/dashboard",
					//templateUrl:"/app/themes/default/templates/dashboard.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
							'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
							},
							'main@': {
								templateUrl: '/app/themes/default/templates/dashboard.tpl.html',
								controller:'dashboardCtrl'
							}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								if(window.adminUser){
									defer.resolve();
								}
								else{
									defer.reject();
								}
							}, 10);
							return defer.promise;
						}]
					}
				}
			)
		.state('users',
				{
					url:"/users",
					//templateUrl:"/app/themes/default/templates/dashboard.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
							'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
							},
							'main@': {
								templateUrl: '/app/themes/default/templates/user.tpl.html',
								controller:'userCtrl'
							}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								if(window.adminUser){
									defer.resolve();
								}
								else{
									defer.reject();
								}
							}, 10);
							return defer.promise;
						}]
					}
				}
			)
		.state('showplants',
				{
					url:"/showplants",
					//templateUrl:"/app/themes/default/templates/dashboard.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
							'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
							},
							'main@': {
								templateUrl: '/app/themes/default/templates/editPlant.tpl.html',
								controller:'showplantCtrl'
							}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								if(window.adminUser){
									defer.resolve();
								}
								else{
									defer.reject();
								}
							}, 10);
							return defer.promise;
						}]
					}
				}
			)
		.state('region',
				{
					url:"/region",
					//templateUrl:"/app/themes/default/templates/dashboard.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
							'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
							},
							'main@': {
								templateUrl: '/app/themes/default/templates/region.tpl.html',
								controller:'regionCtrl'
							}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								if(window.adminUser){
									defer.resolve();
								}
								else{
									defer.reject();
								}
							}, 10);
							return defer.promise;
						}]
					}
				}
			)
		.state('plant',
				{
					url:"/plant",
					templateUrl:"/app/themes/default/templates/plant.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
						'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
						},
						'main@': {
								templateUrl: '/app/themes/default/templates/plant.tpl.html',
								controller:'plantCtrl'
						}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								// if(!window.adminUser){
									defer.resolve();
								/*}
								else{
									defer.reject();
								}*/
							}, 10);
							return defer.promise;
						}]
					}
				}
			);
		$urlRouterProvider.otherwise('/dashboard');	
}])

app.run(["$rootScope","$state",function($rootScope,$state){
		$rootScope.dataPoint = "/getdata";
		$rootScope.$on('$stateChangeError', function(event){
			if(window.adminUser)
			$state.go('dashboard');
			else
			$state.go('plant');
		});
}])