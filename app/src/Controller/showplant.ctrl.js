app.controller('showplantCtrl', [
  '$scope',
  '$rootScope',
  '$http',
  'uiGridConstants',
  '$state',
  '$rootScope',
  function($scope, $rootScope,$http,uiGridConstants,$state,$rootScope) {

    $http.get($rootScope.dataPoint+"?api=plant/plantsCount")
    .success(function (data) {
      $scope.gridOptions.totalItems = data;
  	   $http.get($rootScope.dataPoint+"?api=plant/dashboard/offset/1/limit/"
        +($scope.gridOptions.totalItems > paginationOptions.pageSize ? paginationOptions.pageSize : $scope.gridOptions.totalItems))
       .success(function(res){
  		    $scope.gridOptions.data = res;
  	   });
    });

  	var paginationOptions = {
    	pageNumber: 1,
    	pageSize: 25,
    	sort: null
  	};

     var getPage = function() {

            var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
  
             $http.get($rootScope.dataPoint+"?api=plant/dashboard/offset/"+(firstRow+1)+"/limit/"
              +($scope.gridOptions.totalItems > (firstRow + paginationOptions.pageSize) ? (firstRow + paginationOptions.pageSize) : $scope.gridOptions.totalItems))
             .success(function(res){
                $scope.gridOptions.data = res;
             });
      };

  	$scope.gridOptions = {
  		enableFiltering: false,
    	paginationPageSizes: [25, 50, 75],
    	paginationPageSize: 25,
      useExternalPagination: true,
    	onRegisterApi: function(gridApi){
      		 gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
            paginationOptions.pageNumber = newPage;
            paginationOptions.pageSize = pageSize;
            getPage();
          });
    	},
    	columnDefs:[{field:"plantName",name:"Plant Name",enableSorting:false,enableFiltering:false},
      {field:"townName", name: "Region",enableSorting:false,enableFiltering:false},
      {field:"userCount",name:"No of Users",enableSorting:false,enableFiltering:false},
      {field:"stationCount", name:"No of Stations",enableSorting:false,enableFiltering:false}],
      data:[]
  	};

  }]);