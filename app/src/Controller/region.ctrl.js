app.controller('regionCtrl', [
  '$scope',
  '$rootScope',
  '$http',
  'uiGridConstants',
  '$state',
  function($scope, $rootScope,$http,uiGridConstants,$state) {
  	$http.get($rootScope.dataPoint + "?api=town/dashboard").success(function(res){
  		$scope.gridOptions.data = res;
  	});
  	 var nextWeek = new Date();
  nextWeek.setDate(nextWeek.getDate() + 7);

  	var paginationOptions = {
    	pageNumber: 1,
    	pageSize: 25,
    	sort: null
  	};

    $scope.userClick = function(){
      $state.go('users');
    };

    $scope.plantClick = function(){
      $state.go('showplants');
    };

    $scope.regionClick = function(){
      $state.go('region');
    };

  	$scope.gridOptions = {
  		enableFiltering: true,
    	paginationPageSizes: [25, 50, 75],
    	paginationPageSize: 25,
    	onRegisterApi: function(gridApi){
      		$scope.gridApi = gridApi;
    	},
      columnDefs:[
      {field:"townName", name: "Region Name",enableSorting:false,enableFiltering:false},
      {field:"plantsCount",name:"No of Plants",enableSorting:false,enableFiltering:false},
      {field:"userCount",name:"No of Users",enableSorting:false,enableFiltering:false},
      {field:"stationCout",name:"No of Stations",enableSorting:false,enableFiltering:false}],
      data:[]
  	};

  }]);