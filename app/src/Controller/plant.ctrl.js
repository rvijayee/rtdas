app.controller('plantCtrl', [
  '$scope',
  '$rootScope',
  '$http',
  'uiGridConstants',
  '$state',
  function($scope, $rootScope,$http,uiGridConstants,$state) {
    
   // $scope.userDetails = {};
   // $scope.plantDetails = {};
        $scope.adminUser = window.adminUser;
       $http.get("/getdata?api=user/"+window.rtdas.userId)
       .success(function(res){
          if(res.status){
            $scope.userDetails = null;
          }
          else{
            
            if(res.plantId){
              $scope.userDetails = res;
              $http.get("/getdata?api=station/plant/"+$scope.userDetails.plantId)

              .success(function(data){
                  if(data && data.length > 1){
                      console.log(data);
                      $scope.plantDetails = data;
                  }
                  else{
                    $scope.plantDetails = null;
                  }
              })
              .error(function(){
                    $scope.plantDetails = null;
                   });
            }
            else{
              $scope.userDetails = null;
            }
          }
       }).error(function(){
        $scope.userDetails = null;
       });

    var paginationOptions = {
      pageNumber: 1,
      pageSize: 25,
      sort: null
    };

     var getPage = function() {

            var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
  
             $http.get($rootScope.dataPoint+"/plant/offset/"+(firstRow+1)+"/limit/"
              +($scope.gridOptions.totalItems > (firstRow + paginationOptions.pageSize) ? (firstRow + paginationOptions.pageSize) : $scope.gridOptions.totalItems))
             .success(function(res){
                $scope.gridOptions.data = res;
             });
      };

    $scope.gridOptions = {
      enableFiltering: true,
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      onRegisterApi: function(gridApi){
           gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
            paginationOptions.pageNumber = newPage;
            paginationOptions.pageSize = pageSize;
            getPage();
          });
      },
      columnDefs:[{field:"plantName",name:"Plant Name",enableSorting:false},
      {field:"districtName", name: "District Name",enableSorting:false},
      {field:"plantEmail",name:"Plant Email",enableSorting:false},
      {field:"authPersonName", name:"Authorized Person Name",enableSorting:false}],
      data:[]
    };

  }]);