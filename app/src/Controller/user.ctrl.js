app.controller('userCtrl', [
  '$scope',
  '$rootScope',
  '$http',
  'uiGridConstants',
  '$state',
  function($scope, $rootScope,$http,uiGridConstants,$state) {
  	$http.get("/app/src/data.json").success(function(res){
  		$scope.gridOptions.data = res;
  	});
  	 var nextWeek = new Date();
  nextWeek.setDate(nextWeek.getDate() + 7);

  	var paginationOptions = {
    	pageNumber: 1,
    	pageSize: 25,
    	sort: null
  	};

    $scope.userClick = function(){
      $state.go('users');
    };

    $scope.plantClick = function(){
      $state.go('showplants');
    };

    $scope.regionClick = function(){
      $state.go('region');
    };

  	$scope.gridOptions = {
  		enableFiltering: true,
    	paginationPageSizes: [25, 50, 75],
    	paginationPageSize: 25,
    	onRegisterApi: function(gridApi){
      		$scope.gridApi = gridApi;
    	},
    	columnDefs:[{ field: 'name'},
      // pre-populated search field
      { field: 'gender', enableFiltering: false},
      // no filter input
      { field: 'company', enableFiltering: false, filter: {
        noTerm: true,
        condition: function(searchTerm, cellValue) {
          return cellValue.match(/a/);
        }
      }},
      // specifies one of the built-in conditions
      // and a placeholder for the input
      {
      	name:"Emaiol Id",
        field: 'email',
        filter: {
          condition: uiGridConstants.filter.ENDS_WITH,
          placeholder: 'ends with'
        }
      },
      // custom condition function
      {
        field: 'phone',
        filter: {
          condition: function(searchTerm, cellValue) {
            var strippedValue = (cellValue + '').replace(/[^\d]/g, '');
            return strippedValue.indexOf(searchTerm) >= 0;
          }
        }
      },
      // multiple filters
      { field: 'age', filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        }
      ]}],
      data:[]
  	};

  }]);