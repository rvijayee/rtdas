<!DOCTYPE>
<html ng-app="rtdas">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="icon" type="image/png" href="themes/default/images/logo.png" />
<link rel="stylesheet" href="themes/default/css/bootstrap.min.css">
 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" href="themes/default/css/font.css">
<link rel="stylesheet" href="themes/default/css/cmxform.css">
<link rel="stylesheet" href="themes/default/css/style.css">
<script type="text/javascript" src="vendor/angular/angular.min.js"></script>
<script type="text/javascript" src="dist/rtdas.js"></script>
<script type="text/javascript" src="dist/rtdas-tpls.js"></script>
<title>RT-DAS</title>
</head>

<body ng-controller="rtdasMasterCtrl" ng-click="hello()">
	<div>
		
	</div>
</body>
</html>
