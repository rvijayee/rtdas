module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-html2js');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-string-replace');
  grunt.loadNpmTasks('grunt-contrib-watch');
  

  grunt.initConfig({

    clean: {
      dist: {
        src: ['dist']
      },
      js: {
        src: ['dist/*.js']
      }
    },

    concat: {

      options: {
        // define a string to put between each file in the concatenated output
        separator: ';'
      },

      		"singleOutput": {
			"src": [

        "app/src/app.js",
				"app/src/**/*.mdl.js",
				"app/src/**/*.ctrl.js",
				"app/src/**/*.fltr.js",
				"app/src/**/*.drv.js",
				"app/src/**/*.ftry.js",
			  	"app/src/**/*.srv.js",
				"app/src/**/*.pvr.js",
				"app/src/**/*.val.js",
				"app/src/**/**/*.ctrl.js",
				"app/src/**/**/*.drv.js"


			],
			"dest": "dist/rtdas.js"
		}
    },

    uglify: {
      main: {
        files: [{
          mangle: true,
          expand: true,
          cwd: 'dist',
          src: '*.js',
          dest: 'dist/',
          ext: '.min.js'
        }]
      }
    },

    html2js: {
      options: {
        module: "rtdasTemplates",
        singleModule: true,
        rename: function(moduleName) {
            return moduleName.replace('../', '');
          }
          // custom options, see below
      },

      main: {
        src: [
            'app/themes/default/templates/*.tpl.html',
            //'app/themes/**/templates/**/*.html'
        ],
        dest: 'dist/rtdas-tpls.js'
      }
    },

    watch: {
      dist: {
        files: [
            'app/src/*.js',
            'app/src/**/*.js',
            'app/themes/**/templates/*.html',
          //  'app/themes/**/templates/**/*.html',
        ],
        tasks: [
            'clean:dist',
            'concat',
            'html2js',
            'uglify',
        ],
        options: {
          interrupt: true,
          livereload: true
        }
      },
      js: {
        files: [
          'app/src/*.js',
          'app/src/**/*.js',
          'app/themes/**/templates/*.html',
          //'app/themes/**/templates/**/*.html'
        ],
        tasks: [
          'clean:js',
          'concat',
          'html2js',
          'uglify'
        ],
        options: {
          interrupt: true,
          livereload: true
        }
      }
    }

  });

  grunt.registerTask('default', [
    'clean:dist',
    'concat',
    'html2js',
    'uglify',
    'watch:dist'
  ]);

};
