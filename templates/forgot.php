<?php require 'header.php' ?>


<?php if (!isset($_SESSION['forgot'])): ?>
	<div class="margin-login clearfix">
  <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-4 col-sm-offset-3 col-xs-offset-1 clearfix"> <img class="img-responsive center-block" alt="logo" src="app/themes/default/images/logo.png">
    <div class="backlogin clearfix">
      <div class="paddingg">
        <h1 class="h1login">Forgot Password</h1>
        <p class="plogins">Please enter the following info to reset your RT-DAS password</p>
        <form id="forgot" action="forgot" method="POST" class="margintp">
          <div class="form-group formdesg">
            <label for="exampleInputEmail1">User Name</label>
            <input type="text" class="form-control" id="username" name="username" required placeholder="">
          </div>
          <div class="form-group formdesg">
            <label for="exampleInputPassword1">Email</label>
            <input type="email" class="form-control" id="email" name="email" required placeholder="">
          </div>
          <div class="form-group formdesg">
            <label for="exampleInputPassword1">Mobile No.</label>
            <input type="text" class="form-control" id="mobile" name="mobile" required placeholder="">
          </div>
          <button type="submit" class="btn btn-designsn">Send Reset Token</button>
         
        </form>
      </div>
      <div class="border paddingg"> <a href="#" class="forgotps">Don't have an account? </a><span><a class="registera" href="registration"> Register now </a></span> or <span><a class="registera" href="login"> Login </a></span> </div>
    </div>
  </div>
</div>
<script>
$().ready(function() {
		// validate the comment form when it is submitted
		$("#forgot").validate({
  rules: {
    mobile: {
      required: true,
      minlength: 10,
      maxlength: 10,
    }
  },
  messages: {
  	mobile: {
  	  minlength: 'Mobile number must contain 10 digits',
  	  maxlength: 'Mobile number must contain 10 digits',
  	}
  }
});
});
</script>
<?php elseif($_SESSION['forgot'] == 'token'): ?>
	<div class="margin-login clearfix">
  <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-4 col-sm-offset-3 col-xs-offset-1 clearfix"> <img class="img-responsive center-block" alt="logo" src="app/themes/default/images/logo.png">
    <div class="backlogin clearfix">
      <div class="paddingg">
        <h1 class="h1login">Reset token</h1>
        <p class="plogins">Please enter the token number sent on your email.</p>
        <form id="forgot-token" action="tokenverify" method="POST"  class="margintp">
          <div class="form-group formdesg">
            <label for="exampleInputEmail1">Token No.</label>
            <input type="text" class="form-control" id="token" name="token" required placeholder="">
          </div>
          
          
          <button type="submit" class="btn btn-design">Reset Password</button>
         
        </form>
      </div>
      <div class="border paddingg"> <a href="#" class="forgotps">Don't have an account? </a><span><a class="registera" href="#"> Register now </a></span> or <span><a class="registera" href="#"> Login </a></span> </div>
    </div>
  </div>
</div>
<script>
$().ready(function() {
		// validate the comment form when it is submitted
		$("#forgot-token").validate();
});
</script>
<?php elseif($_SESSION['forgot'] == 'reset'): ?>
	<div class="margin-login clearfix">
  <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-4 col-sm-offset-3 col-xs-offset-1 clearfix"> <img class="img-responsive center-block" alt="logo" src="app/themes/default/images/logo.png">
    <div class="backlogin clearfix">
      <div class="paddingg">
        <h1 class="h1login">Reset Password</h1>
        <p class="plogins">Please enter your new password to login.</p>
        <form id="forgot-reset" action="reset" method="POST" class="margintp">
          <div class="form-group formdesg">
            <label for="exampleInputEmail1">Your new password</label>
            <input type="password" class="form-control" id="password" name="password" required placeholder="">
          </div>
          <div class="form-group formdesg">
            <label for="exampleInputPassword1">Confirm your new password</label>
            <input type="password" class="form-control" id="confirm" name="confirm" required placeholder="">
          </div>
          <button type="submit" class="btn btn-designsn btn-block">Reset Password and Login</button>
        </form>
      </div>
      <div class="border paddingg"> <a href="#" class="forgotps">Don't have an account? </a><span><a class="registera" href="#"> Register now </a></span> </div>
    </div>
  </div>
</div>
<script>
$().ready(function() {
		// validate the comment form when it is submitted
		$("#forgot-reset").validate({
  rules: {
    confirm: {
      equalTo: "#password"
    }
  },
  messages: {
  	confirm: {
  	  equalTo: 'Password doesn\'t match.'
  	}
  }
});
});
</script>
<?php elseif($_SESSION['forgot'] == 'success'): ?>
<div class="margin-login clearfix">
  <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-4 col-sm-offset-3 col-xs-offset-1 clearfix"> <img class="img-responsive center-block" alt="logo" src="app/themes/default/images/logo.png">
    <div class="backlogin clearfix">
      <div class="paddingg">
        <h1 class="h1login">Reset Complete</h1>
        <p class="plogins">Password reset is complete, <a href='login'>Login</a> now.....</p>
        
      </div>
      
    </div>
  </div>
</div>
<?php endif; ?>

<?php require 'footer.php' ?>