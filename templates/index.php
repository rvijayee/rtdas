<!DOCTYPE>
<html ng-app="rtdas">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="icon" type="image/png" href="app/themes/default/images/logo.png" />
<link rel="stylesheet" href="app/themes/default/css/bootstrap.min.css">


<link rel="icon" type="image/png" href="app/themes/default/images/logo.png" />
<link rel="stylesheet" href="app/themes/default/css/bootstrap.min.css">
<link rel="stylesheet" href="app/themes/default/css/ui-grid.css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="app/themes/default/css/font-awesome.min.css">
<link rel="stylesheet" href="app/themes/default/css/font.css">
<link rel="stylesheet" href="app/themes/default/css/bootstrap-select.min.css">
<link rel="stylesheet" href="app/themes/default/css/cmxform.css">
<link rel="stylesheet" href="app/themes/default/css/style.css">
<script src="app/vendor/jquery.min.js"></script>

 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" href="app/themes/default/css/font.css">
<link rel="stylesheet" href="app/themes/default/css/cmxform.css">
<link rel="stylesheet" href="app/themes/default/css/style.css">
<script type="text/javascript" src="app/vendor/angular/angular.min.js"></script>
<script type="text/javascript" src="app/vendor/angular-ui-router/release/angular-ui-router.js"></script>
<script src="app/vendor/bootstrap.min.js"></script>
<script src="app/vendor/jquery.validate.js"></script>
<script src="app/vendor/bootstrap-select.min.js"></script>
<script src="app/vendor/ui-grid.js"></script>
<script type="text/javascript" src="dist/rtdas.js"></script>
<script type="text/javascript" src="dist/rtdas-tpls.js"></script>
<title>RT-DAS</title>
</head>

<script type="text/javascript">
window.rtdas = {
  userrole: '<?php echo $_SESSION['userrole']; ?>',
  userId: '<?php echo $_SESSION['userId']; ?>',
  username: '<?php echo $_SESSION['user'] ?>'
};
window.adminUser = window.rtdas.userrole == "Admin" ? true : false ;
</script>


<body ng-controller="rtdasMasterCtrl">
	 <div data-ui-view='header'> </div>
    <div data-ui-view='main'> </div>
<!-- 	<div class="col-md-12 col-sm-12 white clearfix">
  <nav>
    <div class="col-md-3 col-xs-3 margin"> <img src="app/themes/default/images/logo.png" alt="logo" class="img-responsive logo"> </div>
    <div class="col-md-9 col-xs-9 margins">
      <ul class="navs list-inline marginb-none">
        <li><a href="#">Welcome, Admin</a></li>
        <li><a href="#">Logout</a></li>
      </ul>
    </div>
  </nav>
</div>
	<ui-view></ui-view> -->
</body>

</html>
