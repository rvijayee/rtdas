<?php require 'header.php' ?>

<div class="margin-login clearfix">
  <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-4 col-sm-offset-3 col-xs-offset-1 clearfix"> <img class="img-responsive center-block" alt="logo" src="themes/default/images/logo.png">
    <div class="backlogin clearfix">
      <div class="paddingg">
        <h1 class="h1login">Hello!</h1>
        <p class="plogins">Please login to RT-DAS with your credentials.</p>
        <?php if(!empty($error)):?>
			<p class="error"><?php echo $error?></p>
		<?php endif;?>
        <form action="/login" method="POST" id="registration" class="margintp">
          <div class="form-group formdesg">
            <label for="exampleInputEmail1">User Name</label>
            <input type="text" class="form-control" id="username" name="username" required value="<?php echo $username_value?>" placeholder="">
            <span class="error"><?php echo $username_error?></span>
          </div>
          <div class="form-group formdesg">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="password" name="password" required placeholder="">
            <span class="error"><?php echo $password_error?></span>
          </div>
          <button type="submit" class="btn btn-design">Login</button>
          <a href="forgot" class="forgotpass clearfix">Forgot passowrd?</a>
        </form>
      </div>
      <div class="border paddingg"> <a href="#" class="forgotps">Don't have an account? </a><span><a class="registera" href="/registration"> Register now </a></span> </div>
    </div>
  </div>
</div>
<script>
$().ready(function() {
		// validate the comment form when it is submitted
		$("#registration").validate();
});
</script>

<?php require 'footer.php' ?>