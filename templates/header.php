<html>
<head>
   <style type="text/css">body {font-size:16px;} .error {background-color:red} .small {font-size:12px}</style>
   <link rel="stylesheet" href="app/themes/default/css/bootstrap.min.css">
   <link rel="stylesheet" href="app/themes/default/css/font.css">
   <link rel="stylesheet" href="app/themes/default/css/cmxform.css">
   <link rel="stylesheet" href="app/themes/default/css/style.css">
   <script src="app/vendor/jquery.min.js" type="text/javascript"></script>
   <script type="text/javascript" src="app/vendor/jquery.validate.js"></script>
   <script type="text/javascript">
   $(document).ready(function() {
      $('#login').click(function() {
         $('#login').attr("href", $('#login').attr("href") + "?r=" + encodeURIComponent($(location).attr('pathname')));
      });
   });
   </script>
</head>
<body>

<?if (!empty($user)):?>

   <a href="/">Home</a> | <a href="/about">About</a> | <a href="/level/contact">Deep Contact</a>
   | <a href="/private/about">Private About</a>
   | <a href="/private/goodstuff">Private Good Stuff</a>
   | <a href="/logout">Logout</a>
<?endif;?>

<hr/>