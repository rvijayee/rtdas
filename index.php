<?php
require 'vendor/autoload.php';

$app = new \Slim\Slim();

$app->add(new \Slim\Middleware\SessionCookie(array('secret' => 'myappsecret')));

$authenticate = function ($app) {
    return function () use ($app) {
        if (!isset($_SESSION['user'])) {
            $_SESSION['urlRedirect'] = $app->request()->getPathInfo();
            $app->flash('error', 'Login required');
            $app->redirect('/login');
        }
    };
};

$app->hook('slim.before.dispatch', function() use ($app) { 
   $user = null;
   if (isset($_SESSION['user'])) {
      $user = $_SESSION['user'];
   }
   $app->view()->setData('user', $user);
});

$app->get("/", function () use ($app) {
	if (!isset($_SESSION['user'])) {
		$app->redirect('/login');
	}
	else {
    	$app->render('index.php');
	}
});

$app->get("/about", function () use ($app) {
   $app->render('about.php');
});

$app->get("/level/contact", function () use ($app) {
   $app->render('levelContact.php');
});

$app->get("/logout", function () use ($app) {
   unset($_SESSION['user']);
   $app->view()->setData('user', null);
   //$app->render('logout.php');
   $app->redirect('/login');
});

$app->get("/registration", function () use ($app) {
   if (isset($_SESSION['user'])) {
      $app->redirect('/');
   }
   $app->render('registration.php');
});

$app->get("/forgot", function () use ($app) {
   if (isset($_SESSION['user'])) {
      $app->redirect('/');
   }
   $app->render('forgot.php');
   if ($_SESSION['forgot'] == 'success') {
   	  unset($_SESSION['forgot']);
   }
});

$app->post("/forgot", function () use ($app) {
    $username = $app->request()->post('username');
    $email = $app->request()->post('email');
	$mobile = $app->request()->post('mobile');
	$_SESSION['forgotUser'] = $email;
	// Add the logic to validate the parameters and call the serive to send a token.
	// Also add failure case.
	$_SESSION['forgot'] = 'token';
	$app->redirect('forgot');
});

$app->post("/tokenverify", function () use ($app) {
   // Redirect back to step 1, if we are unable to find anon session
   if (!isset($_SESSION['forgot']) || !isset($_SESSION['forgotUser']) || empty($_SESSION['forgotUser'])) {
      $app->redirect('forgot');
   }
    $email = $_SESSION['forgotUser'];
	$token = $app->request()->post('token');
	// Add the logic to validate the token and to forward on reset
	// Also add failure case.
	$_SESSION['forgot'] = 'reset';
	$app->redirect('forgot');
});

$app->post("/reset", function () use ($app) {
   // Redirect back to step 1, if we are unable to find anon session
   if (!isset($_SESSION['forgot']) || !isset($_SESSION['forgotUser']) || empty($_SESSION['forgotUser'])) {
      $app->redirect('forgot');
   }
    $password = $app->request()->post('password');
	$confirm = $app->request()->post('confirm');
	if ($password != $confirm) {
		$app->redirect('forgot');
	}
	$_SESSION['forgot'] = 'success';
	// Add the logic to forward on reset request to service.
	unset($_SESSION['forgotUser']);
	$app->redirect('forgot');
});

$app->get("/login", function () use ($app) {
   if (isset($_SESSION['user'])) {
      $app->redirect('/');
   }

   $flash = $app->view()->getData('flash');

   $error = '';
   if (isset($flash['error'])) {
      $error = $flash['error'];
   }

   $urlRedirect = '/';

   if ($app->request()->get('r') && $app->request()->get('r') != '/logout' && $app->request()->get('r') != '/login') {
      $_SESSION['urlRedirect'] = $app->request()->get('r');
   }

   if (isset($_SESSION['urlRedirect'])) {
      $urlRedirect = $_SESSION['urlRedirect'];
   }

   $username_value = $username_error = $password_error = '';

   if (isset($flash['username'])) {
      $username_value = $flash['username'];
   }

   if (isset($flash['errors']['username'])) {
      $username_error = $flash['errors']['username'];
   }

   if (isset($flash['errors']['password'])) {
      $password_error = $flash['errors']['password'];
   }

   $app->render('login.php', array('error' => $error, 'username_value' => $username_value, 'username_error' => $username_error, 'password_error' => $password_error, 'urlRedirect' => $urlRedirect));
});

$app->post("/login", function () use ($app) {
    $username = $app->request()->post('username');
    $password = $app->request()->post('password');

    $errors = array();
	$data = array('loginId' => $username, 'password' => $password);                                                                    
    $data_string = json_encode($data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "localhost:8081/user/login");
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
       'Content-Type: application/json',                                                                                
       'Content-Length: ' . strlen($data_string))                                                                       
    );
	
	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);
	
	// If the API is JSON, use json_decode.
	$result = json_decode($raw_data);
    if (empty($result) || !isset($result->loginId)) {
        $errors['username'] = "Please provide valid credentials";
		//echo "in";
    }

	//echo $raw_data;
	//var_dump($result);exit;

    /*if ($username != "brian@nesbot.com") {
        $errors['username'] = "Email is not found.";
    } else if ($password != "aaaa") {
        $app->flash('username', $username);
        $errors['password'] = "Password does not match.";
    }*/

    if (count($errors) > 0) {
        $app->flash('errors', $errors);
        $app->redirect('/login');
    }

    $_SESSION['user'] = $username;
	$_SESSION['userId'] = $result->userId;
	$_SESSION['userrole'] = $result->userType;

    if (isset($_SESSION['urlRedirect'])) {
       $tmp = $_SESSION['urlRedirect'];
       unset($_SESSION['urlRedirect']);
       $app->redirect($tmp);
    }

    $app->redirect('/');
});

$app->get("/getdata", function () use ($app) {
    $api_end = $app->request()->get('api');

    $errors = array();
	//$data = array('loginId' => $username, 'password' => $password);                                                                    
    //$data_string = json_encode($data);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "localhost:8081/" . $api_end);
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
	
	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);

	// If the API is JSON, use json_decode.
	$result = json_decode($raw_data);

   header("Content-Type: application/json");
   echo json_encode($result);
   exit;
});

$app->get("/private/about", $authenticate($app), function () use ($app) {
   $app->render('privateAbout.php');
});

$app->get("/private/goodstuff", $authenticate($app), function () use ($app) {
   $app->render('privateGoodStuff.php');
});


$app->run();