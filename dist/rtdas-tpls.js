angular.module('rtdasTemplates', []).run(['$templateCache', function($templateCache) {
  $templateCache.put("app/themes/default/templates/dashboard.tpl.html",
    "<div class=\"col-md-12 co-sm-12 bodytag clearfix\">\n" +
    "    <div class=\"clearfix\">\n" +
    "      <div class=\"col-lg-8 col-md-12 col-sm-12 paddingnone\">\n" +
    "        <div class=\"col-md-4 col-sm-4 clearfix\" ng-click=\"regionClick()\">\n" +
    "          <div class=\"right padding\"> <span class=\"plogins\">NO. OF REGIONS</span> <a class=\"pull-right registera\">View Regions</a>\n" +
    "            <h1 class=\"h1large\">596</h1>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4 col-sm-4 clearfix\" ng-click=\"plantClick()\">\n" +
    "          <div class=\"right padding\"> <span class=\"plogins\">NO. OF PLANTS</span> <a class=\"pull-right registera\">View Plants</a>\n" +
    "            <h1 class=\"h1large\">1225</h1>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4 col-sm-4 clearfix\" ng-click=\"userClick()\">\n" +
    "          <div class=\"right padding\"> <span class=\"plogins\">NO. OF USERS</span> <a class=\"pull-right registera\">View Users</a>\n" +
    "            <h1 class=\"h1large\">1512</h1>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"col-lg-4 col-md-12 col-sm-12 clearfix\">\n" +
    "        <div class=\"right padding\">\n" +
    "          <p class=\"psearch\">Search</p>\n" +
    "          <div class=\"marginsearch clearfix\">\n" +
    "            <form role=\"search\">\n" +
    "              <div class=\"form-group col-md-10 paddingl-none col-xs-10\">\n" +
    "                <input type=\"text\" class=\"form-control\" placeholder=\"Search Plant Name / Plant ID / User Id\">\n" +
    "              </div>\n" +
    "              <div class=\"col-md-2 col-xs-2 paddingnone clearfix\">\n" +
    "                <button class=\"btn btn-default btn-search\" type=\"submit\">Go</button>\n" +
    "              </div>\n" +
    "            </form>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <div class=\"right bt-marginlr clearfix\">\n" +
    "      <div class=\"padding clearfix\">\n" +
    "        <h1 class=\"h1login\">New users</h1>\n" +
    "        <p class=\"plogins\">The below are the details for the new users created.</p>\n" +
    "      </div>\n" +
    "      <div class=\"borders paddingtp table-responsive dashboard-grid\">\n" +
    "         <div id=\"grid1\" ui-grid=\"gridOptions\" ui-grid-pagination style=\"height:300px;\"></div>\n" +
    "\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <!-- <div class=\"col-lg-12 col-md-12 paddingnone clearfix\">\n" +
    "      <div class=\"col-lg-4 col-md-6 pull-right clearfix\">\n" +
    "        <div class=\"pull-right paddingnone clearfix\">\n" +
    "          <nav class=\"text-right\">\n" +
    "            <ul class=\"pagination marginone\">\n" +
    "              <li><a href=\"#\">1</a></li>\n" +
    "              <li><a href=\"#\">2</a></li>\n" +
    "              <li><a href=\"#\">3</a></li>\n" +
    "              <li><a href=\"#\">4</a></li>\n" +
    "              <li><a href=\"#\">5</a></li>\n" +
    "              <li><a href=\"#\">...</a></li>\n" +
    "              <li><a href=\"#\">250</a></li>\n" +
    "            </ul>\n" +
    "          </nav>\n" +
    "        </div>\n" +
    "      </div> -->\n" +
    "     <!--  <div class=\"col-lg-4 col-md-6 pull-right clearfix\">\n" +
    "        <ul class=\"list-unstyled list-inline text-right\">\n" +
    "          <li class=\"spanpa\" >Records per page</li>\n" +
    "          <li >\n" +
    "            <select class=\"selectpicker spanpa\" data-width=\"70\">\n" +
    "              <option>2</option>\n" +
    "              <option>5</option>\n" +
    "              <option>7</option>\n" +
    "            </select>\n" +
    "          </li>\n" +
    "        </ul>\n" +
    "      </div> -->\n" +
    "    </div>\n" +
    "</div>\n" +
    "</div>\n" +
    "");
  $templateCache.put("app/themes/default/templates/editPlant.tpl.html",
    "<div class=\"col-md-12 co-sm-12 bodytag clearfix\">\n" +
    "    <div class=\"col-md-12 col-sm-12 margintps\">\n" +
    "      <ul class=\"list-inline bredcm\">\n" +
    "        <li><a href=\"#\">Dashboard </a></li>\n" +
    "        <li><a>></a></li>\n" +
    "        <li><a href=\"#\">Nirmal</a></li> \n" +
    "           <li><a>></a></li>\n" +
    "        <li><a href=\"#\">Plants</a></li>\n" +
    "      </ul>\n" +
    "    </div>\n" +
    "    <div class=\"clearfix\">\n" +
    "      <div class=\"col-lg-8 col-md-12 col-sm-12 paddingnone\">\n" +
    "        <div class=\"right bt-marginlr clearfix\">\n" +
    "          <div class=\"padding clearfix\">\n" +
    "            <div class=\"col-md-6 col-sm-12\">\n" +
    "              <h1 class=\"h1login\">Plants</h1>\n" +
    "              <p class=\"plogins\">The below are the details for plants.</p>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-6 margintp col-sm-12\">\n" +
    "              <form role=\"search \">\n" +
    "                <div class=\"form-group col-md-10 paddingl-none col-xs-10\">\n" +
    "                  <input type=\"text\" placeholder=\"Search plants\" class=\"form-control\">\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2 col-xs-2 paddingnone clearfix\">\n" +
    "                  <button type=\"submit\" class=\"btn btn-default btn-search\">Go</button>\n" +
    "                </div>\n" +
    "              </form>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"borders paddingtp table-responsive dashboard-grid\">\n" +
    "            <div ui-grid=\"gridOptions\" ui-grid-pagination style=\"height:300px;\"></div>\n" +
    "          </div>\n" +
    "       <!--  <div class=\"col-lg-12 col-md-12 paddingnone clearfix\">\n" +
    "          <div class=\"col-lg-5 col-md-5 pull-right clearfix\">\n" +
    "            <div class=\"pull-right paddingnone clearfix\">\n" +
    "              <nav class=\"text-right\">\n" +
    "                <ul class=\"pagination marginone\">\n" +
    "                  <li><a href=\"#\">1</a></li>\n" +
    "                  <li><a href=\"#\">2</a></li>\n" +
    "                  <li><a href=\"#\">3</a></li>\n" +
    "                  <li><a href=\"#\">4</a></li>\n" +
    "                  <li><a href=\"#\">5</a></li>\n" +
    "                  <li><a href=\"#\">...</a></li>\n" +
    "                  <li><a href=\"#\">250</a></li>\n" +
    "                </ul>\n" +
    "              </nav>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"col-lg-4 col-md-4 pull-right clearfix\">\n" +
    "            <ul class=\"list-unstyled list-inline text-right\">\n" +
    "              <li class=\"spanpa\" >Records per page</li>\n" +
    "              <li >\n" +
    "                <select class=\"selectpicker spanpa\" data-width=\"70\">\n" +
    "                  <option>2</option>\n" +
    "                  <option>5</option>\n" +
    "                  <option>7</option>\n" +
    "                </select>\n" +
    "              </li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "        </div> -->\n" +
    "      </div>\n" +
    "      <div class=\"col-lg-4 col-md-12 col-sm-12 clearfix\" ng-hide=\"true\">\n" +
    "        <div class=\"right padding\">\n" +
    "          <p class=\"psearch\">Search</p>\n" +
    "          <div class=\"marginsearch clearfix\">\n" +
    "            <form role=\"search\">\n" +
    "              <div class=\"form-group col-md-10 paddingl-none col-xs-10\">\n" +
    "                <input type=\"text\" class=\"form-control\" placeholder=\"Search Plant Name / Plant ID / User Id\">\n" +
    "              </div>\n" +
    "              <div class=\"col-md-2 col-xs-2 paddingnone clearfix\">\n" +
    "                <button class=\"btn btn-default btn-search\" type=\"submit\">Go</button>\n" +
    "              </div>\n" +
    "            </form>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</div>\n" +
    "");
  $templateCache.put("app/themes/default/templates/header.tpl.html",
    "<div class=\"col-md-12 col-sm-12 white clearfix\">\n" +
    "  <nav>\n" +
    "    <div class=\"col-md-3 col-xs-3 margin\"> <img src=\"app/themes/default/images/logo.png\" alt=\"logo\" class=\"img-responsive logo\"> </div>\n" +
    "    <div class=\"col-md-9 col-xs-9 margins\">\n" +
    "      <ul class=\"navs list-inline marginb-none\">\n" +
    "        <li><a href=\"#\">Welcome, {{username}}</a></li>\n" +
    "        <li><a href=\"logout\">Logout</a></li>\n" +
    "      </ul>\n" +
    "    </div>\n" +
    "  </nav>\n" +
    "</div>");
  $templateCache.put("app/themes/default/templates/login.tpl.html",
    "<div class=\"margin-login clearfix\">\n" +
    "  <div class=\"col-md-4 col-sm-6 col-xs-10 col-md-offset-4 col-sm-offset-3 col-xs-offset-1 clearfix\"> <img class=\"img-responsive center-block\" alt=\"logo\" src=\"themes/default/images/logo.png\">\n" +
    "    <div class=\"backlogin clearfix\">\n" +
    "      <div class=\"paddingg\">\n" +
    "        <h1 class=\"h1login\">Hello!</h1>\n" +
    "        <p class=\"plogins\">Please login to RT-DAS with your credentials.</p>\n" +
    "        <form id=\"registration\" class=\"margintp\">\n" +
    "          <div class=\"form-group formdesg\">\n" +
    "            <label for=\"exampleInputEmail1\">User Name</label>\n" +
    "            <input type=\"email\" class=\"form-control\" id=\"\" name=\"um\" required placeholder=\"\">\n" +
    "          </div>\n" +
    "          <div class=\"form-group formdesg\">\n" +
    "            <label for=\"exampleInputPassword1\">Password</label>\n" +
    "            <input type=\"password\" class=\"form-control\" id=\"\" name=\"ps\" required placeholder=\"\">\n" +
    "          </div>\n" +
    "          <button type=\"submit\" class=\"btn btn-design\">Login</button>\n" +
    "          <a href=\"#\" class=\"forgotpass clearfix\">Forgot passowrd?</a>\n" +
    "        </form>\n" +
    "      </div>\n" +
    "      <div class=\"border paddingg\"> <a href=\"#\" class=\"forgotps\">Don't have an account? </a><span><a class=\"registera\" href=\"/registration\"> Register now </a></span> </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>");
  $templateCache.put("app/themes/default/templates/plant.tpl.html",
    "<div class=\"col-md-12 co-sm-12 bodytag clearfix\">\n" +
    "<div class=\"col-md-12 col-sm-12 margintps\" ng-if=\"adminUser\">\n" +
    "      <ul class=\"list-inline bredcm\">\n" +
    "        <li><a href=\"#\">Dashboard </a></li>\n" +
    "        <li><a>></a></li>\n" +
    "      </ul>\n" +
    "    </div>\n" +
    "  <div class=\"col-md-12 col-sm-12 margintps\" ng-if=\"!userDetails\" style=\"text-align:center;font-size:20px;\">\n" +
    "      Sorry user data not available. Please Contact your administrator...\n" +
    "  </div>\n" +
    "  <div class=\"col-md-3 col-sm-4 clearfix\" ng-if=\"userDetails\">\n" +
    "    <div class=\"left\">\n" +
    "      <h1 class=\"h1login\">Plant Info</h1>\n" +
    "      <p class=\"plogins\">Plant info entered while registering with RT-DAS</p>\n" +
    "      <div class=\"clearfix\">\n" +
    "        <div class=\"col-md-6 col-sm-6 margin paddingnone \">\n" +
    "          <p class=\"leftp\">DAS Name</p>\n" +
    "          <a class=\"leftsmallp\" ng-bind=\"userDetails.dasName ? userDetails.dasName : 'NA'\">Silver DAS</a> </div>\n" +
    "        <div class=\"col-md-6 col-sm-6 margin paddingnone \">\n" +
    "          <p class=\"leftp\" >IP Address of DAS</p>\n" +
    "          <a class=\"leftsmallp\" ng-bind=\"userDetails.dasIpAddress ? userDetails.dasIpAddress : 'NA'\">192.168.1.1</a> </div>\n" +
    "        <div class=\"col-md-6 col-sm-6 margin-lite paddingnone \">\n" +
    "          <p class=\"leftp\">MAC Address of DAS</p>\n" +
    "          <a class=\"leftsmallp\" ng-bind=\"userDetails.dasMacAddress ? userDetails.dasMacAddress : 'NA'\">255.168.2.0</a> </div>\n" +
    "      </div>\n" +
    "      <div class=\"border margin clearfix\">\n" +
    "        <div class=\"col-md-6 col-sm-6 margin paddingnone \">\n" +
    "          <p class=\"leftp\">User ID</p>\n" +
    "          <a class=\"leftsmallp\" ng-bind=\"userDetails.userId ? userDetails.userId : 'NA'\">navneet19</a> </div>\n" +
    "        <div class=\"col-md-6 col-sm-6 margin paddingnone \">\n" +
    "          <p class=\"leftp\">Plant ID</p>\n" +
    "          <a class=\"leftsmallp\" ng-bind=\"userDetails.plantId ? userDetails.plantId : 'NA'\">#980283</a> </div>\n" +
    "        <div class=\"col-md-6 col-sm-6 margin-lite paddingnone \">\n" +
    "          <p class=\"leftp\">Plant Name</p>\n" +
    "          <a class=\"leftsmallp\" ng-bind=\"userDetails.plantName ? userDetails.plantName : 'NA'\">Nirmal Indo power Plant</a> </div>\n" +
    "        <div class=\"col-md-6 col-sm-6 margin-lite paddingnone \">\n" +
    "          <p class=\"leftp\">Zone</p>\n" +
    "          <a class=\"leftsmallp\" ng-bind=\"userDetails.zoneName ? userDetails.zoneName : 'NA'\">Nirmal</a> </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <div class=\"lefts\"  ng-if=\"adminUser\">\n" +
    "    <div class=\"paddings\">\n" +
    "      <h1 class=\"h1login\">Plant Users</h1>\n" +
    "      <p class=\"plogins\">Below are thr users attached to the plant</p>\n" +
    "      <div class=\"clearfix\">\n" +
    "        <div class=\"col-md-10 col-sm-12 margin-lyte paddingnone \">\n" +
    "        <input type=\"text\" class=\"form-control\" placeholder=\"Search User ID\">\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      \n" +
    "    </div>\n" +
    "    <div class=\"margin clearfix\">\n" +
    "        <div class=\"list-group\">\n" +
    "  <a class=\"list-group-item green-active\" href=\"#\"><i class=\"fa fa-caret-right\"></i>&nbsp; Navneet19 <i class=\"fa fa-times pull-right\"></i></a>\n" +
    "  <a class=\"list-group-item no-active\" href=\"#\">vijay912 <i class=\"fa fa-times pull-right\"></i></a>\n" +
    " \n" +
    "</div>\n" +
    "      </div>\n" +
    "      </div>\n" +
    "      <div class=\"col-md-6 col-sm-6 paddingnone pull-right\" ng-if=\"adminUser\">\n" +
    "      <button class=\"btn btn-upload btn-block marginbtm\">Add New User</button>\n" +
    "      </div>\n" +
    "  </div>\n" +
    "  <div class=\"col-md-9 col-sm-8 clearfix\" ng-if=\"userDetails && plantDetails\">\n" +
    "    <form>\n" +
    "      <div class=\"right clearfix\">\n" +
    "        <div class=\"padding clearfix\">\n" +
    "          <h1 class=\"h1login\" ng-bind=\"adminUser?'Upload Data' : 'Station Details'\"></h1>\n" +
    "          <p class=\"plogins\">Please fill the below form to add station details.</p>\n" +
    "        </div>\n" +
    "        <div class=\"borders clearfix\">\n" +
    "          <div class=\"col-md-6 col-sm-6 plleft\">\n" +
    "            <p class=\"plogins\">Current number of stations in the plant.</p>\n" +
    "            <div class=\"form-group col-md-4 col-xs-4 widfull text-center formdesg2\">\n" +
    "              <label>No. of Stacks</label>\n" +
    "              <h4>2</h4>\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-xs-4 widfull formdesg2 text-center \">\n" +
    "              <label>No. of CEMS</label>\n" +
    "              <h4>2</h4>\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-xs-4 widfull formdesg2 text-center \">\n" +
    "              <label>No. of EQMS</label>\n" +
    "             <h4>0</h4>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"col-md-6 col-sm-6 plright\">\n" +
    "            <p class=\"plogins\">Number of stations planning to connect in future.</p>\n" +
    "            <div class=\"form-group col-md-4 col-xs-4 widfull formdesg2 text-center \">\n" +
    "              <label>No. of Stacks</label>\n" +
    "              <h4>0</h4>\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-xs-4 widfull formdesg2 text-center \">\n" +
    "              <label>No. of CEMS</label>\n" +
    "             <h4>0</h4>\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-xs-4 widfull formdesg2 text-center \">\n" +
    "              <label>No. of EQMS</label>\n" +
    "              <h4>0</h4>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"right clearfix\">\n" +
    "        <div class=\"paddingtp table-responsive\">\n" +
    "          <table class=\"table tabledesg\">\n" +
    "            <thead>\n" +
    "              <tr>\n" +
    "                <th> Station Type </th>\n" +
    "                <th> Parameter Name </th>\n" +
    "                <th> Unit </th>\n" +
    "                <th> Low Range </th>\n" +
    "                <th> High Range </th>\n" +
    "                <th> Nomenclature </th>\n" +
    "                <th> Location </th>\n" +
    "                <th> Attached to </th>\n" +
    "                <th> Output </th>\n" +
    "              \n" +
    "              </tr>\n" +
    "            </thead>\n" +
    "            <tbody>\n" +
    "              <tr ng-repeat=\"plant in plantDetails\">\n" +
    "                <td ng-bind=\"plant.stationType ? plant.stationType : 'NA'\"></td>\n" +
    "                <td ng-bind=\"plant.parameters ? plant.parameters : 'NA'\"></td>\n" +
    "                <td ng-bind=\"plant.status ? plant.status : 'NA'\">Mg/m3</td>\n" +
    "                <td ng-bind=\"plant.parameters ? plant.parameters : 'NA'\">0</td>\n" +
    "                <td ng-bind=\"plant.parameters ? plant.parameters : 'NA'\">1000</td>\n" +
    "                <td ng-bind=\"plant.parameters ? plant.parameters : 'NA'\">Stack 1</td>\n" +
    "                <td ng-bind=\"plant.location ? plant.location : 'NA'\">Near Gate</td>\n" +
    "                <td ng-bind=\"plant.gprsDeviceId ? plant.gprsDeviceId : 'NA'\">Coke Oven</td>\n" +
    "                <td ng-bind=\"plant.parameters ? plant.parameters : 'NA'\">RS232</td>\n" +
    "              \n" +
    "              </tr>\n" +
    "            </tbody>\n" +
    "          </table>\n" +
    "          <!-- <div id=\"grid1\" ui-grid=\"gridOptions\" ui-grid-pagination style=\"height:300px;\"></div> -->\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"col-md-12 col-sm-12 paddingnone text-right marginbtm clearfix\">\n" +
    "        <button class=\"btn btn-modify\" ng-if=\"adminUser\">Stop</button>\n" +
    "        <button class=\"btn btn-modify\" ng-if=\"adminUser\">Upload</button>\n" +
    "        <button class=\"btn btn-modify\" ng-click=\"enableModify=!enableModify\">Modify</button>&nbsp;&nbsp;\n" +
    "       \n" +
    "      </div>\n" +
    "      <div class=\"right clearfix\" ng-show=\"enableModify\">\n" +
    "        <div class=\"padding clearfix\">\n" +
    "          <p class=\"plogins bt-padding margin-lyte\">Please mention the detail about the station that you want to transmit to SPCB</p>\n" +
    "        \n" +
    "        \n" +
    "            <div class=\"col-lg-6 col-md-12 col-sm-12 paddingnone\">\n" +
    "            <div class=\"form-group col-md-4 col-sm-4 formdesg2\">\n" +
    "              <label>Station Type </label>\n" +
    "              <div class=\"btn-group bootstrap-select\" style=\"width: 150px;\"><button type=\"button\" class=\"btn dropdown-toggle btn-default\" data-toggle=\"dropdown\" title=\"CEMS\"><span class=\"filter-option pull-left\">CEMS</span>&nbsp;<span class=\"bs-caret\"><span class=\"caret\"></span></span></button><div class=\"dropdown-menu open\"><ul class=\"dropdown-menu inner\" role=\"menu\"><li data-original-index=\"0\" class=\"selected\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">CEMS</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li><li data-original-index=\"1\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">value 1</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li><li data-original-index=\"2\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">value 2</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li></ul></div><select class=\"selectpicker\" data-width=\"150\" tabindex=\"-98\">\n" +
    "                <option>CEMS</option>\n" +
    "                <option>value 1</option>\n" +
    "                <option>value 2</option>\n" +
    "              </select></div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-sm-4 formdesg2\">\n" +
    "              <label>Parameter Name </label>\n" +
    "              <div class=\"btn-group bootstrap-select\" style=\"width: 150px;\"><button type=\"button\" class=\"btn dropdown-toggle btn-default\" data-toggle=\"dropdown\" title=\"PM\"><span class=\"filter-option pull-left\">PM</span>&nbsp;<span class=\"bs-caret\"><span class=\"caret\"></span></span></button><div class=\"dropdown-menu open\"><ul class=\"dropdown-menu inner\" role=\"menu\"><li data-original-index=\"0\" class=\"selected\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">PM</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li><li data-original-index=\"1\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">value 1</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li><li data-original-index=\"2\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">value 2</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li></ul></div><select class=\"selectpicker\" data-width=\"150\" tabindex=\"-98\">\n" +
    "                <option>PM</option>\n" +
    "                <option>value 1</option>\n" +
    "                <option>value 2</option>\n" +
    "              </select></div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-sm-4 formdesg2\">\n" +
    "              <label>Unit </label>\n" +
    "              <div class=\"btn-group bootstrap-select\" style=\"width: 150px;\"><button type=\"button\" class=\"btn dropdown-toggle btn-default\" data-toggle=\"dropdown\" title=\"Mg/m3\"><span class=\"filter-option pull-left\">Mg/m3</span>&nbsp;<span class=\"bs-caret\"><span class=\"caret\"></span></span></button><div class=\"dropdown-menu open\"><ul class=\"dropdown-menu inner\" role=\"menu\"><li data-original-index=\"0\" class=\"selected\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">Mg/m3</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li><li data-original-index=\"1\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">value 1</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li><li data-original-index=\"2\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">value 2</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li></ul></div><select class=\"selectpicker\" data-width=\"150\" tabindex=\"-98\">\n" +
    "                <option>Mg/m3</option>\n" +
    "                <option>value 1</option>\n" +
    "                <option>value 2</option>\n" +
    "              </select></div>\n" +
    "            </div>\n" +
    "            </div>\n" +
    "               \n" +
    "        \n" +
    "            <div class=\"col-lg-6 col-md-12 col-sm-12 paddingnoness\">\n" +
    "            <div class=\"form-group col-md-4 col-sm-4 formdesg2\">\n" +
    "              <label>Low Range</label>\n" +
    "              <input type=\"text\" class=\"form-control\" id=\"\" value=\"0\" placeholder=\"\">\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-sm-4 formdesg2\">\n" +
    "              <label>High Range</label>\n" +
    "              <input type=\"text\" class=\"form-control\" id=\"\" value=\"1000\" placeholder=\"\">\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-sm-4 formdesg2\">\n" +
    "              <label>Nomenclature</label>\n" +
    "              <div class=\"btn-group bootstrap-select\" style=\"width: 150px;\"><button type=\"button\" class=\"btn dropdown-toggle btn-default\" data-toggle=\"dropdown\" title=\"Stack 1\"><span class=\"filter-option pull-left\">Stack 1</span>&nbsp;<span class=\"bs-caret\"><span class=\"caret\"></span></span></button><div class=\"dropdown-menu open\"><ul class=\"dropdown-menu inner\" role=\"menu\"><li data-original-index=\"0\" class=\"selected\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">Stack 1</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li><li data-original-index=\"1\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">value 1</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li><li data-original-index=\"2\"><a tabindex=\"0\" class=\"\" style=\"\" data-tokens=\"null\"><span class=\"text\">value 2</span><span class=\"glyphicon glyphicon-ok check-mark\"></span></a></li></ul></div><select class=\"selectpicker\" data-width=\"150\" tabindex=\"-98\">\n" +
    "                <option>Stack 1</option>\n" +
    "                <option>value 1</option>\n" +
    "                <option>value 2</option>\n" +
    "              </select></div>\n" +
    "            </div>\n" +
    "            </div>\n" +
    "            \n" +
    "            <div class=\"col-lg-6 col-md-12 col-sm-12 paddingnone\">\n" +
    "            <div class=\"form-group col-md-4 col-sm-4 formdesg2\">\n" +
    "              <label>Location </label>\n" +
    "              <input type=\"text\" class=\"form-control\" id=\"\" placeholder=\"\">\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-sm-4 formdesg2\">\n" +
    "              <label>Attached to </label>\n" +
    "              <input type=\"text\" class=\"form-control\" id=\"\" placeholder=\"\">\n" +
    "            </div>\n" +
    "            <div class=\"form-group col-md-4 col-sm-4 formdesg2\">\n" +
    "              <label>Output </label>\n" +
    "              <input type=\"text\" class=\"form-control\" id=\"\" value=\"RS232\" placeholder=\"\">\n" +
    "            </div>\n" +
    "          </div>\n" +
    "               <div class=\"col-lg-6 col-md-12 col-sm-12 paddingnoness\">\n" +
    "            <div class=\"form-group col-md-12 col-sm-12 formdesg2\">\n" +
    "              <button class=\"btn btn-designs\">Add</button>\n" +
    "              <button class=\"btn btn-designs2\">Clear</button>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </div>\n" +
    "\n" +
    "</div>\n" +
    "");
  $templateCache.put("app/themes/default/templates/region.tpl.html",
    "<div class=\"col-md-12 co-sm-12 bodytag clearfix\">\n" +
    "    <div class=\"col-md-12 col-sm-12 margintps\">\n" +
    "      <ul class=\"list-inline bredcm\">\n" +
    "        <li><a href=\"#\">Dashboard </a></li>\n" +
    "        <li><a>></a></li>\n" +
    "        <li><a href=\"#\">Regions</a></li>\n" +
    "      </ul>\n" +
    "    </div>\n" +
    "    <div class=\"clearfix\">\n" +
    "      <div class=\"col-lg-8 col-md-12 col-sm-12 paddingnone\">\n" +
    "        <div class=\"right bt-marginlr clearfix\">\n" +
    "          <div class=\"padding clearfix\">\n" +
    "            <div class=\"col-md-6 col-sm-12\">\n" +
    "              <h1 class=\"h1login\">Regions</h1>\n" +
    "              <p class=\"plogins\">The below are the details of regions.</p>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-6 margintp col-sm-12\">\n" +
    "              <form role=\"search \">\n" +
    "                <div class=\"form-group col-md-10 paddingl-none col-xs-10\">\n" +
    "                  <input type=\"text\" placeholder=\"Search regions\" class=\"form-control\">\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2 col-xs-2 paddingnone clearfix\">\n" +
    "                  <button type=\"submit\" class=\"btn btn-default btn-search\">Go</button>\n" +
    "                </div>\n" +
    "              </form>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"borders paddingtp table-responsive dashboard-grid\">\n" +
    "            <!-- <table class=\"table tabledesg\">\n" +
    "              <thead>\n" +
    "                <tr>\n" +
    "                  <th class=\"fv wdtab\"> Region Name </th>\n" +
    "                  <th> No. of plants </th>\n" +
    "                  <th> No. of Users </th>\n" +
    "                  <th> No. of Stations </th>\n" +
    "                </tr>\n" +
    "              </thead>\n" +
    "              <tbody>\n" +
    "                <tr>\n" +
    "                  <td class=\"green fv wdtab\"><a href=\"#\">Tamilnadu</a></td>\n" +
    "                  <td>35</td>\n" +
    "                  <td>250</td>\n" +
    "                  <td>400</td>\n" +
    "                </tr>\n" +
    "                <tr>\n" +
    "                  <td class=\"green fv wdtab\"><a href=\"#\">Kerela</a></td>\n" +
    "                  <td>27</td>\n" +
    "                  <td>175</td>\n" +
    "                  <td>305</td>\n" +
    "                </tr>\n" +
    "              </tbody>\n" +
    "            </table> -->\n" +
    "            <div ui-grid=\"gridOptions\" ui-grid-pagination style=\"height:300px;\"></div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "       <!--  <div class=\"col-lg-12 col-md-12 paddingnone clearfix\">\n" +
    "          <div class=\"col-lg-5 col-md-5 pull-right clearfix\">\n" +
    "            <div class=\"pull-right paddingnone clearfix\">\n" +
    "              <nav class=\"text-right\">\n" +
    "                <ul class=\"pagination marginone\">\n" +
    "                  <li><a href=\"#\">1</a></li>\n" +
    "                  <li><a href=\"#\">2</a></li>\n" +
    "                  <li><a href=\"#\">3</a></li>\n" +
    "                  <li><a href=\"#\">4</a></li>\n" +
    "                  <li><a href=\"#\">5</a></li>\n" +
    "                  <li><a href=\"#\">...</a></li>\n" +
    "                  <li><a href=\"#\">250</a></li>\n" +
    "                </ul>\n" +
    "              </nav>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"col-lg-4 col-md-4 pull-right clearfix\">\n" +
    "            <ul class=\"list-unstyled list-inline text-right\">\n" +
    "              <li class=\"spanpa\" >Records per page</li>\n" +
    "              <li >\n" +
    "                <select class=\"selectpicker spanpa\" data-width=\"70\">\n" +
    "                  <option>2</option>\n" +
    "                  <option>5</option>\n" +
    "                  <option>7</option>\n" +
    "                </select>\n" +
    "              </li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "        </div> -->\n" +
    "      </div>\n" +
    "      <div class=\"col-lg-4 col-md-12 col-sm-12 clearfix\" ng-hide=\"true\">\n" +
    "        <div class=\"right padding\">\n" +
    "          <p class=\"psearch\">Search</p>\n" +
    "          <div class=\"marginsearch clearfix\">\n" +
    "            <form role=\"search\">\n" +
    "              <div class=\"form-group col-md-10 paddingl-none col-xs-10\">\n" +
    "                <input type=\"text\" class=\"form-control\" placeholder=\"Search Plant Name / Plant ID / User Id\">\n" +
    "              </div>\n" +
    "              <div class=\"col-md-2 col-xs-2 paddingnone clearfix\">\n" +
    "                <button class=\"btn btn-default btn-search\" type=\"submit\">Go</button>\n" +
    "              </div>\n" +
    "            </form>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</div>\n" +
    "");
  $templateCache.put("app/themes/default/templates/user.tpl.html",
    "<div class=\"clearfix\">\n" +
    "      <div class=\"col-lg-8 col-md-12 col-sm-12 paddingnone\">\n" +
    "        <div class=\"right bt-marginlr clearfix\">\n" +
    "          <div class=\"padding clearfix\">\n" +
    "            <div class=\"col-md-6 col-sm-12\">\n" +
    "              <h1 class=\"h1login\">Users</h1>\n" +
    "              <p class=\"plogins\">The below are the details for plants.</p>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-6 margintp col-sm-12\">\n" +
    "              <form role=\"search \">\n" +
    "                <div class=\"form-group col-md-10 paddingl-none col-xs-10\">\n" +
    "                  <input type=\"text\" placeholder=\"Search User Name/ User Id\" class=\"form-control\">\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2 col-xs-2 paddingnone clearfix\">\n" +
    "                  <button type=\"submit\" class=\"btn btn-default btn-search\">Go</button>\n" +
    "                </div>\n" +
    "              </form>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"borders paddingtp table-responsive dashboard-grid\">\n" +
    "         <!--    <table class=\"table tabledesg\">\n" +
    "              <thead>\n" +
    "                <tr>\n" +
    "                  <th class=\"fv\">  Plant Name    </th>\n" +
    "                  <th> Region </th>                                                                                                  \n" +
    "                  <th> No. of Users </th>\n" +
    "                  <th> No. of Stations </th>\n" +
    "                </tr>\n" +
    "              </thead>\n" +
    "              <tbody>\n" +
    "                <tr>\n" +
    "                  <td class=\"green fv\"><a href=\"#\">INDO POWER PLANT</a></td>\n" +
    "                  <td>Nirmal</td>\n" +
    "                  <td>5</td>\n" +
    "                  <td>10</td>\n" +
    "                </tr>\n" +
    "                <tr>\n" +
    "                  <td class=\"green fv\"><a href=\"#\">SRI SAI PLANT</a></td>\n" +
    "                  <td>Nirmal</td>\n" +
    "                  <td>7</td>\n" +
    "                  <td>9</td>\n" +
    "                </tr>\n" +
    "              </tbody>\n" +
    "            </table> -->\n" +
    "            <div id=\"grid1\" ui-grid=\"gridOptions\" ui-grid-pagination style=\"height:300px;\"></div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <!-- <div class=\"col-lg-12 col-md-12 paddingnone clearfix\">\n" +
    "          <div class=\"col-lg-5 col-md-5 pull-right clearfix\">\n" +
    "            <div class=\"pull-right paddingnone clearfix\">\n" +
    "              <nav class=\"text-right\">\n" +
    "                <ul class=\"pagination marginone\">\n" +
    "                  <li><a href=\"#\">1</a></li>\n" +
    "                  <li><a href=\"#\">2</a></li>\n" +
    "                  <li><a href=\"#\">3</a></li>\n" +
    "                  <li><a href=\"#\">4</a></li>\n" +
    "                  <li><a href=\"#\">5</a></li>\n" +
    "                  <li><a href=\"#\">...</a></li>\n" +
    "                  <li><a href=\"#\">250</a></li>\n" +
    "                </ul>\n" +
    "              </nav>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"col-lg-4 col-md-4 pull-right clearfix\">\n" +
    "            <ul class=\"list-unstyled list-inline text-right\">\n" +
    "              <li class=\"spanpa\" >Records per page</li>\n" +
    "              <li >\n" +
    "                <select class=\"selectpicker spanpa\" data-width=\"70\">\n" +
    "                  <option>2</option>\n" +
    "                  <option>5</option>\n" +
    "                  <option>7</option>\n" +
    "                </select>\n" +
    "              </li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "        </div> -->\n" +
    "      </div>\n" +
    "      <div class=\"col-lg-4 col-md-12 col-sm-12 clearfix\" ng-hide=\"true\">\n" +
    "        <div class=\"right padding\">\n" +
    "          <p class=\"psearch\">Search</p>\n" +
    "          <div class=\"marginsearch clearfix\">\n" +
    "            <form role=\"search\">\n" +
    "              <div class=\"form-group col-md-10 paddingl-none col-xs-10\">\n" +
    "                <input type=\"text\" class=\"form-control\" placeholder=\"Search Plant Name / Plant ID / User Id\">\n" +
    "              </div>\n" +
    "              <div class=\"col-md-2 col-xs-2 paddingnone clearfix\">\n" +
    "                <button class=\"btn btn-default btn-search\" type=\"submit\">Go</button>\n" +
    "              </div>\n" +
    "            </form>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</div>\n" +
    "");
}]);
