window.adminUser = true;
var app = angular.module('rtdas',["ui.router","ui.grid","rtdasTemplates","ui.grid.pagination"]).config(["$stateProvider","$urlRouterProvider",
	function($stateProvider,$urlRouterProvider){

	$stateProvider

		.state('dashboard',
				{
					url:"/dashboard",
					//templateUrl:"/app/themes/default/templates/dashboard.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
							'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
							},
							'main@': {
								templateUrl: '/app/themes/default/templates/dashboard.tpl.html',
								controller:'dashboardCtrl'
							}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								if(window.adminUser){
									defer.resolve();
								}
								else{
									defer.reject();
								}
							}, 10);
							return defer.promise;
						}]
					}
				}
			)
		.state('users',
				{
					url:"/users",
					//templateUrl:"/app/themes/default/templates/dashboard.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
							'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
							},
							'main@': {
								templateUrl: '/app/themes/default/templates/user.tpl.html',
								controller:'userCtrl'
							}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								if(window.adminUser){
									defer.resolve();
								}
								else{
									defer.reject();
								}
							}, 10);
							return defer.promise;
						}]
					}
				}
			)
		.state('showplants',
				{
					url:"/showplants",
					//templateUrl:"/app/themes/default/templates/dashboard.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
							'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
							},
							'main@': {
								templateUrl: '/app/themes/default/templates/editPlant.tpl.html',
								controller:'showplantCtrl'
							}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								if(window.adminUser){
									defer.resolve();
								}
								else{
									defer.reject();
								}
							}, 10);
							return defer.promise;
						}]
					}
				}
			)
		.state('region',
				{
					url:"/region",
					//templateUrl:"/app/themes/default/templates/dashboard.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
							'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
							},
							'main@': {
								templateUrl: '/app/themes/default/templates/region.tpl.html',
								controller:'regionCtrl'
							}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								if(window.adminUser){
									defer.resolve();
								}
								else{
									defer.reject();
								}
							}, 10);
							return defer.promise;
						}]
					}
				}
			)
		.state('plant',
				{
					url:"/plant",
					templateUrl:"/app/themes/default/templates/plant.tpl.html",
					//controller:"rtdasMasterCtrl",
					views:{
						'header@': {
								templateUrl: '/app/themes/default/templates/header.tpl.html'
						},
						'main@': {
								templateUrl: '/app/themes/default/templates/plant.tpl.html',
								controller:'plantCtrl'
						}
					},
					resolve:{
						adminUser:["$q","$timeout",function($q,$timeout){
							var defer = $q.defer();
							$timeout(function() {
								// if(!window.adminUser){
									defer.resolve();
								/*}
								else{
									defer.reject();
								}*/
							}, 10);
							return defer.promise;
						}]
					}
				}
			);
		$urlRouterProvider.otherwise('/dashboard');	
}])

app.run(["$rootScope","$state",function($rootScope,$state){
		$rootScope.dataPoint = "/getdata";
		$rootScope.$on('$stateChangeError', function(event){
			if(window.adminUser)
			$state.go('dashboard');
			else
			$state.go('plant');
		});
}]);
/**
 * Creates master module for RTDAS. 
 */
// angular.module('rtdas', [
//   'rtdasTemplates'
// ]);

app;app.controller('dashboardCtrl', [
  '$scope',
  '$rootScope',
  '$http',
  'uiGridConstants',
  '$state',
  function($scope, $rootScope,$http,uiGridConstants,$state) {
  	$http.get("/app/src/data.json").success(function(res){
  		$scope.gridOptions.data = res;
  	});
  	 var nextWeek = new Date();
  nextWeek.setDate(nextWeek.getDate() + 7);

  	var paginationOptions = {
    	pageNumber: 1,
    	pageSize: 25,
    	sort: null
  	};

    $scope.userClick = function(){
      $state.go('users');
    };

    $scope.plantClick = function(){
      $state.go('showplants');
    };

    $scope.regionClick = function(){
      $state.go('region');
    };

  	$scope.gridOptions = {
  		enableFiltering: true,
    	paginationPageSizes: [25, 50, 75],
    	paginationPageSize: 25,
    	onRegisterApi: function(gridApi){
      		$scope.gridApi = gridApi;
    	},
    	columnDefs:[{ field: 'name'},
      // pre-populated search field
      { field: 'gender', enableFiltering: false},
      // no filter input
      { field: 'company', enableFiltering: false, filter: {
        noTerm: true,
        condition: function(searchTerm, cellValue) {
          return cellValue.match(/a/);
        }
      }},
      // specifies one of the built-in conditions
      // and a placeholder for the input
      {
      	name:"Emaiol Id",
        field: 'email',
        filter: {
          condition: uiGridConstants.filter.ENDS_WITH,
          placeholder: 'ends with'
        }
      },
      // custom condition function
      {
        field: 'phone',
        filter: {
          condition: function(searchTerm, cellValue) {
            var strippedValue = (cellValue + '').replace(/[^\d]/g, '');
            return strippedValue.indexOf(searchTerm) >= 0;
          }
        }
      },
      // multiple filters
      { field: 'age', filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        }
      ]}],
      data:[]
  	};

  }]);;/**
 * 
 */
app.controller('rtdasMasterCtrl', [
  '$scope',
  '$rootScope',
  function($scope, $rootScope) {
  	$scope.username = window.rtdas.username;
  }]);;app.controller('plantCtrl', [
  '$scope',
  '$rootScope',
  '$http',
  'uiGridConstants',
  '$state',
  function($scope, $rootScope,$http,uiGridConstants,$state) {
    
   // $scope.userDetails = {};
   // $scope.plantDetails = {};
        $scope.adminUser = window.adminUser;
       $http.get("/getdata?api=user/"+window.rtdas.userId)
       .success(function(res){
          if(res.status){
            $scope.userDetails = null;
          }
          else{
            
            if(res.plantId){
              $scope.userDetails = res;
              $http.get("/getdata?api=station/plant/"+$scope.userDetails.plantId)

              .success(function(data){
                  if(data && data.length > 1){
                      console.log(data);
                      $scope.plantDetails = data;
                  }
                  else{
                    $scope.plantDetails = null;
                  }
              })
              .error(function(){
                    $scope.plantDetails = null;
                   });
            }
            else{
              $scope.userDetails = null;
            }
          }
       }).error(function(){
        $scope.userDetails = null;
       });

    var paginationOptions = {
      pageNumber: 1,
      pageSize: 25,
      sort: null
    };

     var getPage = function() {

            var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
  
             $http.get($rootScope.dataPoint+"/plant/offset/"+(firstRow+1)+"/limit/"
              +($scope.gridOptions.totalItems > (firstRow + paginationOptions.pageSize) ? (firstRow + paginationOptions.pageSize) : $scope.gridOptions.totalItems))
             .success(function(res){
                $scope.gridOptions.data = res;
             });
      };

    $scope.gridOptions = {
      enableFiltering: true,
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      onRegisterApi: function(gridApi){
           gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
            paginationOptions.pageNumber = newPage;
            paginationOptions.pageSize = pageSize;
            getPage();
          });
      },
      columnDefs:[{field:"plantName",name:"Plant Name",enableSorting:false},
      {field:"districtName", name: "District Name",enableSorting:false},
      {field:"plantEmail",name:"Plant Email",enableSorting:false},
      {field:"authPersonName", name:"Authorized Person Name",enableSorting:false}],
      data:[]
    };

  }]);;app.controller('regionCtrl', [
  '$scope',
  '$rootScope',
  '$http',
  'uiGridConstants',
  '$state',
  function($scope, $rootScope,$http,uiGridConstants,$state) {
  	$http.get($rootScope.dataPoint + "?api=town/dashboard").success(function(res){
  		$scope.gridOptions.data = res;
  	});
  	 var nextWeek = new Date();
  nextWeek.setDate(nextWeek.getDate() + 7);

  	var paginationOptions = {
    	pageNumber: 1,
    	pageSize: 25,
    	sort: null
  	};

    $scope.userClick = function(){
      $state.go('users');
    };

    $scope.plantClick = function(){
      $state.go('showplants');
    };

    $scope.regionClick = function(){
      $state.go('region');
    };

  	$scope.gridOptions = {
  		enableFiltering: true,
    	paginationPageSizes: [25, 50, 75],
    	paginationPageSize: 25,
    	onRegisterApi: function(gridApi){
      		$scope.gridApi = gridApi;
    	},
      columnDefs:[
      {field:"townName", name: "Region Name",enableSorting:false,enableFiltering:false},
      {field:"plantsCount",name:"No of Plants",enableSorting:false,enableFiltering:false},
      {field:"userCount",name:"No of Users",enableSorting:false,enableFiltering:false},
      {field:"stationCout",name:"No of Stations",enableSorting:false,enableFiltering:false}],
      data:[]
  	};

  }]);;app.controller('showplantCtrl', [
  '$scope',
  '$rootScope',
  '$http',
  'uiGridConstants',
  '$state',
  '$rootScope',
  function($scope, $rootScope,$http,uiGridConstants,$state,$rootScope) {

    $http.get($rootScope.dataPoint+"?api=plant/plantsCount")
    .success(function (data) {
      $scope.gridOptions.totalItems = data;
  	   $http.get($rootScope.dataPoint+"?api=plant/dashboard/offset/1/limit/"
        +($scope.gridOptions.totalItems > paginationOptions.pageSize ? paginationOptions.pageSize : $scope.gridOptions.totalItems))
       .success(function(res){
  		    $scope.gridOptions.data = res;
  	   });
    });

  	var paginationOptions = {
    	pageNumber: 1,
    	pageSize: 25,
    	sort: null
  	};

     var getPage = function() {

            var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
  
             $http.get($rootScope.dataPoint+"?api=plant/dashboard/offset/"+(firstRow+1)+"/limit/"
              +($scope.gridOptions.totalItems > (firstRow + paginationOptions.pageSize) ? (firstRow + paginationOptions.pageSize) : $scope.gridOptions.totalItems))
             .success(function(res){
                $scope.gridOptions.data = res;
             });
      };

  	$scope.gridOptions = {
  		enableFiltering: false,
    	paginationPageSizes: [25, 50, 75],
    	paginationPageSize: 25,
      useExternalPagination: true,
    	onRegisterApi: function(gridApi){
      		 gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
            paginationOptions.pageNumber = newPage;
            paginationOptions.pageSize = pageSize;
            getPage();
          });
    	},
    	columnDefs:[{field:"plantName",name:"Plant Name",enableSorting:false,enableFiltering:false},
      {field:"townName", name: "Region",enableSorting:false,enableFiltering:false},
      {field:"userCount",name:"No of Users",enableSorting:false,enableFiltering:false},
      {field:"stationCount", name:"No of Stations",enableSorting:false,enableFiltering:false}],
      data:[]
  	};

  }]);;app.controller('userCtrl', [
  '$scope',
  '$rootScope',
  '$http',
  'uiGridConstants',
  '$state',
  function($scope, $rootScope,$http,uiGridConstants,$state) {
  	$http.get("/app/src/data.json").success(function(res){
  		$scope.gridOptions.data = res;
  	});
  	 var nextWeek = new Date();
  nextWeek.setDate(nextWeek.getDate() + 7);

  	var paginationOptions = {
    	pageNumber: 1,
    	pageSize: 25,
    	sort: null
  	};

    $scope.userClick = function(){
      $state.go('users');
    };

    $scope.plantClick = function(){
      $state.go('showplants');
    };

    $scope.regionClick = function(){
      $state.go('region');
    };

  	$scope.gridOptions = {
  		enableFiltering: true,
    	paginationPageSizes: [25, 50, 75],
    	paginationPageSize: 25,
    	onRegisterApi: function(gridApi){
      		$scope.gridApi = gridApi;
    	},
    	columnDefs:[{ field: 'name'},
      // pre-populated search field
      { field: 'gender', enableFiltering: false},
      // no filter input
      { field: 'company', enableFiltering: false, filter: {
        noTerm: true,
        condition: function(searchTerm, cellValue) {
          return cellValue.match(/a/);
        }
      }},
      // specifies one of the built-in conditions
      // and a placeholder for the input
      {
      	name:"Emaiol Id",
        field: 'email',
        filter: {
          condition: uiGridConstants.filter.ENDS_WITH,
          placeholder: 'ends with'
        }
      },
      // custom condition function
      {
        field: 'phone',
        filter: {
          condition: function(searchTerm, cellValue) {
            var strippedValue = (cellValue + '').replace(/[^\d]/g, '');
            return strippedValue.indexOf(searchTerm) >= 0;
          }
        }
      },
      // multiple filters
      { field: 'age', filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        }
      ]}],
      data:[]
  	};

  }]);